import React, { Component } from 'react';

import { Text, StyleSheet } from 'react-native';

import { Container, Thumbnail, Content, Button, StyleProvider, Icon } from 'native-base';

import getTheme from '../native-base-theme/components';
import material from '../native-base-theme/variables/material';

import { NavigationActions, StackActions } from 'react-navigation';

import PropTypes from 'prop-types';


const pushAction = StackActions.push({
    routeName: 'Oefeningen',
});

export default class Sidebar extends Component {
    

    render() {
        const profile_pic = require('../assets/profile.png');
        const { navigate } = this.props.navigation;
        const { closeDrawer } = this.props.navigation;
        return (
            <Container>
                <StyleProvider style={getTheme(material)}>
                    <Content style={{backgroundColor:'#FFFFFF'}}>
                    
                        <Thumbnail style={{ alignSelf: "center", marginTop: 30, marginBottom: 5 }} large source={ profile_pic } />
                        
                        {this.isLoggedIn()}

                        <Button style={signupStyles.nestedButtonView} light full large onPress={() => navigate('Home')}>
                            <Icon style={signupStyles.iconLeft} type="FontAwesome" name="home" primary/>
                            <Text style={signupStyles.buttonText}>Home</Text>
                        </Button>

                        <Button style={signupStyles.nestedButtonView} light full large onPress={() => navigate('DashboardScreen')}>
                            <Icon style={signupStyles.iconLeft} type="FontAwesome" name="dashboard" primary />
                            <Text style={signupStyles.buttonText}>Dashboard</Text>
                        </Button>

                        <Button style={signupStyles.nestedButtonView} light full large onPress={() => navigate('Oefeningen')}>
                            <Icon style={signupStyles.iconLeft} type="FontAwesome" name="superpowers" primary/>
                            <Text style={signupStyles.buttonText}>Oefeningen</Text>
                        </Button>

                        <Button style={signupStyles.nestedButtonView} light full large onPress={() => navigate('Sportprogramma')}>
                            <Icon style={signupStyles.iconLeft} type="FontAwesome" name="superpowers" primary  />
                            <Text style={signupStyles.buttonText}>Sportprogramma's</Text>
                        </Button>

                        <Button style={signupStyles.nestedButtonView} light full large onPress={() => navigate('Settings')}>
                            <Icon style={signupStyles.iconLeft} type="FontAwesome" name="gears" primary />
                            <Text style={signupStyles.buttonText}>Settings</Text>
                        </Button>
                    </Content>
                </StyleProvider>
            </Container>
        );
    }

    isLoggedIn(){
        const login = true;
        const { navigate } = this.props.navigation;
        if (login == true){
            return <Button style={{ marginBottom: 15 }} transparent  full large onPress={() => navigate('Login')}>
                <Text>Vincent Venhuizenen</Text>
            </Button>
        } else {
            return <Button style={{ marginBottom: 15 }} transparent  full large onPress={() => navigate('Register')}>
                <Text>Regristreren</Text>
            </Button>
        }
    }
}

Sidebar.propTypes = {
    navigation: PropTypes.object
  };

var signupStyles = StyleSheet.create({
    nestedButtonView: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    
    iconLeft: {
        width: 40,  
    },
    
    buttonText: {
        flex: 1,
        paddingLeft: 20,
        textAlign: 'left',
        fontWeight: 'bold',
    },
});

module.exports = Sidebar;
