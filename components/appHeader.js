import React, { Component } from 'react';

import { Text } from 'react-native';

import {Header,Left,Button,Icon,Right,Body,Title} from 'native-base';

import { Font, AppLoading } from "expo";

export default class AppHeader extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  async componentWillMount() {
      await Font.loadAsync({
          Roboto: require("native-base/Fonts/Roboto.ttf"),
          Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
      });
      this.setState({ loading: false });
  }


  render() {
    if (this.state.loading) {
      return (
        <AppLoading />
      );
    }
    return (
      <Header>
        <Left>
          <Button transparent onPress={()=>this.props.test()} > <Icon name='menu' /> </Button>
        </Left>
        <Body>
          <Title>Oxigym</Title>
        </Body>

      </Header>
    );
  }
}

module.exports = AppHeader;