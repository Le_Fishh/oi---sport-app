import React, { Component } from 'react';
import { Container, Content, Form, Item, Input, Label, Button, Text, Tab, Tabs, Header, StyleProvider, Accordion , View } from 'native-base';
import { Font, AppLoading } from "expo";

import getTheme from '../native-base-theme/components';
import commonColor from '../native-base-theme/variables/commonColor';

import OefeningenTab1 from './tabs/oefeningen_tab1';
import OefeningenTab2 from './tabs/oefeningen_tab2';

const dataArray = [
  { title: "Variant 1", content: "Lorem ipsum dolor sit amet" },
  { title: "Variant 2", content: "Lorem ipsum dolor sit amet" },
  { title: "Variant 3", content: "Lorem ipsum dolor sit amet" }
];

export default class Oefeningen extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  static navigationOptions = ({ navigation }) => ({
    title: `Oefening: ${navigation.state.params.oefeningTitle}`,
      headerTitleStyle : {textAlign: 'center',alignSelf:'center'},
        headerStyle:{
            backgroundColor:'white',
        },
    });

  async componentWillMount() {
      await Font.loadAsync({
          Roboto: require("native-base/Fonts/Roboto.ttf"),
          Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
      });
      this.setState({ loading: false });
  }

  

  render() {
    if (this.state.loading) {
      return (
        <AppLoading />
      );
    }
    return (
      <Container style={{ padding: 50 }} >
          <View >
            <Text>
            Fusce convallis metus id felis luctus adipiscing. Pellentesque egestas, neque sit amet convallis pulvinar, justo nulla eleifend augue, ac auctor orci leo non est. Quisque id mi. Ut tincidunt tincidunt erat. Etiam feugiat lorem non metus. Vestibulum dapibus nunc ac augue. Curabitur vestibulum aliquam leo. 
            </Text>
          </View>

          {this.moreVarianten()}

          <StyleProvider style={getTheme(commonColor)}>
          <View style={{ paddingTop: 50 }}>

            <Text>Aantal keer uitgevoerd deze maand:</Text>

            <View  style={{ flexDirection: "row", paddingTop: 5 }}>
            <Button primary rounded small ><Text style={{ fontWeight: "bold" }}>-</Text></Button>
              <Text style={{ paddingLeft: 5, paddingRight: 5, fontSize: 24, marginTop: -2 }}> 0 </Text>
            <Button primary rounded small ><Text style={{ fontWeight: "bold" }}>+</Text></Button>
            </View>
          </View>
          </StyleProvider>
    </Container>
    );
  }

  moreVarianten(){

    if (dataArray.length < 1){
      return <View style={{ paddingTop: 25 }}>
      <Text> { this.props.navigation.state.params.oefeningId } </Text>
      <Accordion dataArray={dataArray} icon="add" expandedIcon="remove" />
  </View>
    } else {
      return <View style={{ paddingTop: 25 }}>
      <Text> { this.props.navigation.state.params.oefeningId } </Text>
        <Text>
            { dataArray[0].content }
          </Text>
      </View>
    }
}
}
