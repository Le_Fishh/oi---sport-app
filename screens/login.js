import React, { Component } from 'react';
import {  } from 'react-native';
import { Container, Content, Form, Item, Input, Label, Button, Text, StyleProvider } from 'native-base';
import { Font, AppLoading } from "expo";
import { NavigationActions } from 'react-navigation';
import getTheme from '../native-base-theme/components';
import commonColor from '../native-base-theme/variables/commonColor';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  async componentWillMount() {
      await Font.loadAsync({
          Roboto: require("native-base/Fonts/Roboto.ttf"),
          Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
      });
      this.setState({ loading: false });
  }

  navigateToScreen = (route) => () => {
    const navigate = NavigationActions.navigate({
        routeName: route
    });
    this.props.navigation.dispatch(navigate);
  }
  
  state = {
    inlognaam: '',
    password: '',
  }
  handleInlognaam = (text) => {
    this.setState({ inlognaam: text })
  }
  handlePassword = (text) => {
    this.setState({ password: text })
  }
  login = (inlognaam, password) => {
    alert('inlognaam: ' + inlognaam + ' password: ' + password);
  }

  render() {
    if (this.state.loading) {
      return (
        <AppLoading />
      );
    }
    return (
      <Container>
        <StyleProvider style={getTheme(commonColor)}>
      <Content contentContainerStyle={{ paddingTop: 50, marginLeft: 10, marginRight: 10 }}>
        <Form>
          <Item floatingLabel style={{ marginLeft: 10, marginRight: 10, }}>
            <Label>Username</Label>
            <Input onChangeText={this.handleInlognaam}/>
          </Item>
          <Item floatingLabel style={{ marginLeft: 10, marginRight: 10, }}>
            <Label>Password</Label>
            <Input secureTextEntry={true} onChangeText={this.handlePassword}/>
          </Item>
          <Button onPress = {() => this.login(this.state.inlognaam, this.state.password)} primary rounded  block style={{ padding: 10, marginTop: 40, marginLeft: 10, marginRight: 10, }}><Text>Login</Text></Button>
          <Text style={{ padding: 10, marginTop: 20, marginLeft: 10, marginRight: 10, textAlign: 'center'  }}>Heb je nog geen account? Klik dan op regristreren.</Text>
          <Button onPress={this.navigateToScreen('RegisterScreen')} bordered primary rounded  block style={{ padding: 10, marginTop: 10, marginLeft: 10, marginRight: 10, }}><Text>Registreren</Text></Button>
        </Form>
      </Content>
      </StyleProvider>
    </Container>
    );
  }
}
