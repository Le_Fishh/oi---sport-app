import React, { Component } from 'react';
import { Container, Card, CardItem, Form, Body, Item, Input, Label, Button, Text, View, StyleProvider } from 'native-base';
import { Font, AppLoading } from "expo";
import getTheme from '../native-base-theme/components';
import commonColor from '../native-base-theme/variables/commonColor';

export default class SportProgramma extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  async componentWillMount() {
    await Font.loadAsync({
        Roboto: require("native-base/Fonts/Roboto.ttf"),
        Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });
    this.setState({ loading: false });
  }

  handlePassword = (text) => {
    this.setState({ password: text })
  }
  
  
  render() {
    if (this.state.loading) {
      return (
        <AppLoading />
      );
    }
    return (
    <Container style={{ padding: 20 }}>
      
          <Text style={{ fontWeight: 'bold', paddingLeft: 10, fontSize: 24, color: "#00ced1"  }}>Bekijk sportprogramma</Text>
        
      
    </Container>
    );
  }
}
