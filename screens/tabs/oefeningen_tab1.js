import React, { Component } from 'react';
import { Container, Content, Form, Item, Input, Label, Button, Text, Tab, Tabs, Card, CardItem, Body, View  } from 'native-base';
import { Font, AppLoading } from "expo";

import { createDrawerNavigator, createAppContainer, createStackNavigator, NavigationActions, StackActions, Transitioner } from "react-navigation";

export default class OefeningenTab1 extends Component {
  render() {
    
    return (
      <Container style={{ padding: 20 }}>
        <Card transparent>
            <CardItem header >
              <View style={{ flex:1}}>
                <Text style={{textAlignVertical: "center",textAlign: "left", fontWeight: "bold"}}>Oefening titel</Text>
              </View>
              <View style={{ flex:1}}> 
                <Text style={{textAlignVertical: "center",textAlign: "right",  fontWeight: "bold", color: "#00ced1", marginRight: 10}}>Varianten: 1</Text>
              </View>
            </CardItem>
            <CardItem>
              <Body>
                <Text>
                Fusce convallis metus id felis luctus adipiscing. Pellentesque egestas, neque sit amet convallis pulvinar, justo nulla eleifend augue, ac auctor orci leo non est. Quisque id mi. Ut tincidunt tincidunt erat. Etiam feugiat lorem non metus. Vestibulum dapibus nunc ac augue. Curabitur vestibulum aliquam leo. 
                </Text>
              </Body>
            </CardItem>
            <CardItem footer button onPress={() => navigate('Oefening')}>
              <Text style={{ backgroundColor: "#00ced1", paddingLeft: 15, paddingRight: 15, paddingTop: 5, paddingBottom: 5, borderRadius: 5, color: "white"}}>Bekijk</Text>
            </CardItem>
        </Card>
    </Container>
    );
  }
}
