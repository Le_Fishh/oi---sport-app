import React, { Component } from 'react';
import { Container, Card, CardItem, Form, Body, Item, Input, Label, Button, CheckBox, Text, View, StyleProvider, Icon, List, Right, Left, Radio, ListItem } from 'native-base';
import { ListView, ScrollView } from 'react-native';
import { Font, AppLoading } from "expo";
import getTheme from '../native-base-theme/components';
import commonColor from '../native-base-theme/variables/commonColor';

// import AtoZListView from 'react-native-atoz-listview';
import Search from 'react-native-search-box';

var REQUEST_URL = 'https://facebook.github.io/react-native/movies.json';

const datas = [
  'Simon Mignolet',
  'Nathaniel Clyne',
  'Dejan Lovren',
  'Mama Sakho',
  'Alberto Moreno',
  'Emre Can',
  'Joe Allen',
  'Phil Coutinho',
];

const newData = [];

export default class SportProgramma extends Component {
  constructor(props) {   
    super(props);
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      basic: true,
      listViewData: datas,
      loading: true, 
      programmaTitle: this.props.navigation.state.params.programmaTitle
    };
    this.state = {
      //Lets initialize results with the same struct we expect to receive from the api
      results: {
        title: '',
        description: '',
        movies: []
      }
    };
    //Using ES6 we need to bind methods to access 'this'
    this.fetchData = this.fetchData.bind(this);
  }

  async componentWillMount() {
    await Font.loadAsync({
        Roboto: require("native-base/Fonts/Roboto.ttf"),
        Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });
    this.setState({ loading: false });
  }

  
  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    fetch(REQUEST_URL)
      .then((response) => response.json())
      .then((responseData) => {
        this.setState({
          results: responseData
        });
      })
      .done();
  }

 

  renderRow = (item, sectionId, index) => {
    return (
      <TouchableHightLight
        style={{
          height: rowHeight,
          justifyContent: 'center',
          alignItems: 'center'}}
      >
        <Text>{item.name}</Text>
      </TouchableHightLight>
    );
  }

  // Important: You must return a Promise
  beforeFocus = () => {
      return new Promise((resolve, reject) => {
          console.log('beforeFocus');
          resolve();
      });
  }

  // Important: You must return a Promise
  onFocus = (text) => {
      return new Promise((resolve, reject) => {
          console.log('onFocus', text);
          resolve();
      });
  }

  // Important: You must return a Promise
  afterFocus = () => {
      return new Promise((resolve, reject) => {
          console.log('afterFocus');
          resolve();
      });
  }

  render() {
    contents = this.state.results.movies.map((item) => {
      //We need to return the corresponding mapping for each item too.
      return (
          <CardItem>
            <Text> {item.title}</Text>
            <Right>
              <CheckBox color={"#00ced1"}/>
            </Right>
          </CardItem>
        );
    });

    const { navigate } = this.props.navigation;
    if (this.state.loading) {
      return (
        <AppLoading />
      );
    }
    return (
    <Container style={{ padding: 20 }}>
          <Form>
            <Item>
              <Input placeholder={"Sportprogramma naam"} onChangeText={this.handleSportprogramma} defaultValue={this.state.programmaTitle}/>
            </Item>

            <Card style={{ marginTop: 20 }}>
              <Search
                  style={{ marginTop: 40, paddingTop: 40 }}
                  ref="search_box"
                  placeholder="zoeken"
                  backgroundColor="#ffffff"
                  titleCancelColor="#000000"
                  cancelTitle="stop"
                  cancelButtonTextStyle={{
                    textAlign: 'center',
                    
                  }}
                />
                
              <View style={{height: 200}} >
                <ScrollView  scrollEnabled={true}>
                
                  {contents}
                  
                </ScrollView>
              </View>
            </Card>

            <Text style={{ fontWeight: 'bold', marginTop: 20, marginBottom: 10, fontSize: 24, color: "#00ced1"  }}>Notificatie instellen</Text>
            
            
            <ListItem>
              <Left>
                <Text>Geef notificaties</Text>
              </Left>
              <Right>
                <Radio color={"#00ced1"}/>
              </Right>
            </ListItem>
            <Card transparent>
              <CardItem>
                <Text>Dagen:</Text>
              </CardItem>
              <CardItem>
                <Text>Tijd:</Text>
              </CardItem>
            </Card>
            {/* <StyleProvider style={getTheme(commonColor)}>
            <Button primary rounded style={{ padding: 10, marginTop: 10, marginLeft: 10, marginRight: 10, }}><Text>Aanmaken</Text></Button>
            </StyleProvider> */}
          </Form>
          
        
    </Container>
    );
  }
}
