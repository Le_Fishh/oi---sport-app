import React, { Component } from 'react';
import { Container, Card, CardItem, Form, Body, Item, Input, Label, Button, Text, View, StyleProvider } from 'native-base';
import { Font, AppLoading } from "expo";
import getTheme from '../native-base-theme/components';
import commonColor from '../native-base-theme/variables/commonColor';

export default class SportProgramma extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  async componentWillMount() {
    await Font.loadAsync({
        Roboto: require("native-base/Fonts/Roboto.ttf"),
        Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });
    this.setState({ loading: false });
  }

  handlePassword = (text) => {
    this.setState({ password: text })
  }
  
  
  render() {
    if (this.state.loading) {
      return (
        <AppLoading />
      );
    }
    return (
    <Container style={{ padding: 20 }}>
      
          <Text style={{ fontWeight: 'bold', paddingLeft: 10, fontSize: 24, color: "#00ced1"  }}>Nieuw sportprogramma</Text>
          <Form>
            <Item fixedLabel>
              <Label>Nieuw sportprogramma</Label>
              <Input onChangeText={this.handleSportprogramma}/>
            </Item>
            <StyleProvider style={getTheme(commonColor)}>
            <Button primary rounded style={{ padding: 10, marginTop: 10, marginLeft: 10, marginRight: 10, }}><Text>Aanmaken</Text></Button>
            </StyleProvider>
          </Form>
          <Text style={{ fontWeight: 'bold', paddingLeft: 10, marginTop: 40, fontSize: 24, color: "#00ced1", paddingBottom: 10  }}>Mijn sportprogramma's</Text>
          <View>
            {this.allSportprogrammas()}
          </View>
        
    </Container>
    );
  }

  allSportprogrammas(){
    return <Card>
      <CardItem header>
        <Text style={{ fontWeight: '400', fontSize: 24 }}>Sportprogramma naam</Text>
      </CardItem>
      <CardItem>
              <Body>
                <Text style={{ fontWeight: '300', fontSize: 22 }}>
                  Info:
                </Text>
                <Text>
                  Aantal keer uitgevoerd:
                </Text>
                <Text>
                  Aantal oefeningen:
                </Text>
              </Body>
            </CardItem>
            <CardItem footer >
              <StyleProvider style={getTheme(commonColor)}>
                <Button primary rounded>
                  <Text style={{ textAlign: "center", width: 200}}>Bekijk</Text>
                </Button>
              </StyleProvider>
              <StyleProvider style={getTheme(commonColor)}>
                <Button  primary rounded style={{ marginLeft: 20 }}>
                  <Text style={{ textAlign: "center", width: 200}}>Start</Text>
                </Button>
              </StyleProvider>
            </CardItem>
            
    </Card>
  }
}
