import React, { Component } from 'react';
import { FlatList, ActivityIndicator } from 'react-native';
import { Container, Card, CardItem, Form, Body, Item, Input, Label, Button, Text, View, StyleProvider } from 'native-base';
import { Font, AppLoading } from "expo";
import getTheme from '../native-base-theme/components';
import commonColor from '../native-base-theme/variables/commonColor';

export default class SportProgramma extends Component {
  
  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  async componentWillMount() {
    await Font.loadAsync({
        Roboto: require("native-base/Fonts/Roboto.ttf"),
        Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });
    this.setState({ loading: false });
    
  }
  
  

  state = {
    nieuwProgramma: '',
  }
  handleSportprogramma = (text) => {
    this.setState({ nieuwProgramma: text })
  }
  makeProgramma = (nieuwProgramma) => {
    if (nieuwProgramma && nieuwProgramma.length) {
      this.props.navigation.navigate('nieuwProgramma', { programmaTitle: nieuwProgramma})
    } else {
      alert("Vul een titel in!")
    }
  }
  
  render() {
    const { navigate } = this.props.navigation;
    if (this.state.loading) {
      return (
        <AppLoading />
      );
    }
    return (
    <Container style={{ padding: 20 }}>
      
          <Text style={{ fontWeight: 'bold', paddingLeft: 10, fontSize: 24, color: "#00ced1"  }}>Nieuw sportprogramma</Text>
          <Form>
            <Item>
              <Input  placeholder={"Sportprogramma naam"}  onChangeText={this.handleSportprogramma}/>
            </Item>
            <StyleProvider style={getTheme(commonColor)}>
            <Button primary rounded style={{ padding: 10, marginTop: 10, marginLeft: 10, marginRight: 10, }} onPress={() => this.makeProgramma(this.state.nieuwProgramma)}><Text>Aanmaken</Text></Button>
            </StyleProvider>
          </Form>
          <Text style={{ fontWeight: 'bold', paddingLeft: 10, marginTop: 40, fontSize: 24, color: "#00ced1", paddingBottom: 10  }}>Mijn sportprogramma's</Text>
          <View>
            {this.allSportprogrammas()}
          </View>
        
    </Container>
    );
  }

  allSportprogrammas(){
    const { navigate } = this.props.navigation;
    return <Card>
      <CardItem header>
        <Text style={{ fontWeight: '400', fontSize: 24 }}>Sportprogramma naam</Text>
      </CardItem>
      <CardItem>
              <Body>
                <Text style={{ fontWeight: '300', fontSize: 22 }}>
                  Info:
                </Text>
                <Text>
                  Aantal keer uitgevoerd:
                </Text>
                <Text>
                  Aantal oefeningen:
                </Text>
              </Body>
            </CardItem>
            <CardItem footer >
              <StyleProvider style={getTheme(commonColor)}>
                <Button primary rounded onPress={() => this.props.navigation.navigate('bekijkProgramma', { programmaTitle: this.state.niewProgramma})}>
                  <Text style={{ textAlign: "center" }}>Bekijk</Text>
                </Button>
              </StyleProvider>
              <StyleProvider style={getTheme(commonColor)}>
                <Button  primary rounded style={{ marginLeft: 20 }} onPress={() => navigate('startProgramma')}>
                  <Text style={{ textAlign: "center" }}>Start</Text>
                </Button>
              </StyleProvider>
            </CardItem>
            
    </Card>
  }
  
}
