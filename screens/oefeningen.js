import React, { Component } from 'react';
import { Container, Content, Form, Item, Body, View, Button, Text, Tab, Tabs, CardItem, TabHeading, Card } from 'native-base';
import { Font, AppLoading } from "expo";

import { ScrollView } from "react-native";

import { createStackNavigator, createAppContainer } from "react-navigation";

import { Entypo } from '@expo/vector-icons';

import OefeningenTab1 from './tabs/oefeningen_tab1';
import OefeningenTab2 from './tabs/oefeningen_tab2';

var REQUEST_URL = 'https://facebook.github.io/react-native/movies.json';

export default class Oefeningen extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
    this.state = {
      //Lets initialize results with the same struct we expect to receive from the api
      results: {
        title: '',
        description: '',
        movies: []
      }
    };
    //Using ES6 we need to bind methods to access 'this'
    this.fetchData = this.fetchData.bind(this);
  }

  async componentWillMount() {
      await Font.loadAsync({
          Roboto: require("native-base/Fonts/Roboto.ttf"),
          Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
      });
      this.setState({ loading: false });
  }
  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    fetch(REQUEST_URL)
      .then((response) => response.json())
      .then((responseData) => {
        this.setState({
          results: responseData
        });
      })
      .done();
  }

  render() {
    contents = this.state.results.movies.map((item) => {
      //We need to return the corresponding mapping for each item too.
      return (
            <Card transparent>
                    <CardItem header >
                      <View style={{ flex:1}}>
                        <Text style={{textAlignVertical: "center",textAlign: "left", fontWeight: "bold"}}>{item.title}</Text>
                      </View>
                      <View style={{ flex:1}}> 
                        <Text style={{textAlignVertical: "center",textAlign: "right",  fontWeight: "bold", color: "#00ced1", marginRight: 10}}>Varianten: 1</Text>
                      </View>
                    </CardItem>
                    <CardItem>
                      <Body>
                        <Text>
                        Fusce convallis metus id felis luctus adipiscing. Pellentesque egestas, neque sit amet convallis pulvinar, justo nulla eleifend augue, ac auctor orci leo non est. Quisque id mi. Ut tincidunt tincidunt erat. Etiam feugiat lorem non metus. Vestibulum dapibus nunc ac augue. Curabitur vestibulum aliquam leo. 
                        </Text>
                      </Body>
                    </CardItem>
                    <CardItem footer button onPress={() => navigate('SecondScreen', { oefeningId: Math.floor(Math.random() * 100), oefeningTitle: "een oefening"})}>
                      <Text style={{ backgroundColor: "#00ced1", paddingLeft: 15, paddingRight: 15, paddingTop: 5, paddingBottom: 5, borderRadius: 5, color: "white"}}>Bekijk</Text>
                    </CardItem>
                </Card>
        );
    });

    const { navigate } = this.props.navigation;
    if (this.state.loading) {
      return (
        <AppLoading />
      );
    }
    
    return (
      <Container >
        <ScrollView  scrollEnabled={true}>
        <Tabs tabBarUnderlineStyle={{ backgroundColor: "#00ced1" }}>

          <Tab heading={ <TabHeading style={{backgroundColor: 'white'}}><Text style={{ color: "black", fontSize: 18 }}>Normale oefeningen</Text></TabHeading>} >
              <Container style={{ padding: 20 }}>
              {contents}
            </Container>
          </Tab>
          
          <Tab heading={ <TabHeading style={{backgroundColor: 'white'}}><Text style={{ color: "black", fontSize: 18 }}>Speciale oefeningen</Text></TabHeading>} >
            <Container style={{ padding: 20 }}>
              <Card transparent>
                      <CardItem header >
                        <View style={{ flex:1}}>
                          <Text style={{textAlignVertical: "center",textAlign: "left", fontWeight: "bold"}}>Oefening titel</Text>
                        </View>
                        <View style={{ flex:1}}> 
                          <Text style={{textAlignVertical: "center",textAlign: "right",  fontWeight: "bold", color: "#00ced1", marginRight: 10}}>Varianten: 1</Text>
                        </View>
                      </CardItem>
                      <CardItem>
                        <Body>
                          <Text>
                          Fusce convallis metus id felis luctus adipiscing. Pellentesque egestas, neque sit amet convallis pulvinar, justo nulla eleifend augue, ac auctor orci leo non est. Quisque id mi. Ut tincidunt tincidunt erat. Etiam feugiat lorem non metus. Vestibulum dapibus nunc ac augue. Curabitur vestibulum aliquam leo. 
                          </Text> 
                        </Body>
                      </CardItem>
                      <CardItem footer button onPress={() => navigate('SecondScreen', { oefeningId: Math.floor(Math.random() * 100), oefeningTitle: "een andere oefening"})}>
                        <Text style={{ backgroundColor: "#00ced1", paddingLeft: 15, paddingRight: 15, paddingTop: 5, paddingBottom: 5, borderRadius: 5, color: "white"}}>Bekijk</Text>
                      </CardItem>
                  </Card>
              </Container>
          </Tab>
        </Tabs>
        </ScrollView>
    </Container>
    );
  }
}

