import React, { Component } from 'react';
import {  } from 'react-native';
import { Container, Content, Form, Item, Input, Label, Button, Text } from 'native-base';
import { Font, AppLoading } from "expo";

export default class Register extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  async componentWillMount() {
      await Font.loadAsync({
          Roboto: require("native-base/Fonts/Roboto.ttf"),
          Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
      });
      this.setState({ loading: false });
  }


  state = {
    inlognaam: '',
    password: '',
    confirmpassword: '',
    gebruikersnaam: '',
    email: '',
  }
  handleInlognaam = (text) => {
    this.setState({ inlognaam: text })
  }
  handlePassword = (text) => {
    this.setState({ password: text })
  }
  handleConfirmpassword = (text) => {
    this.setState({ confirmpassword: text })
  }
  handleGebruikersnaam = (text) => {
    this.setState({ gebruikersnaam: text })
  }
  handleEmail = (text) => {
      this.setState({ email: text })
  }
  register = (inlognaam, password, confirmpassword, gebruikersnaam, email) => {
      if (password == confirmpassword) {
      alert('inlognaam: ' + inlognaam + ' email: ' + email + ' password: ' + password + ' confirmpassword: ' + confirmpassword + ' gebruikersnaam: ' + gebruikersnaam)
      }else {
        alert('Wachtwoorden komen niet overeen');
      }
  }

  render() {
    if (this.state.loading) {
      return (
        <AppLoading />
      );
    }
    return (
      <Container>
      <Content contentContainerStyle={{ paddingTop: 50, marginLeft: 10, marginRight: 10 }}>
      <Text style={{ padding: 10, marginTop: 20, marginLeft: 10, marginRight: 10, textAlign: 'center'  }}>Vul hieronder je gegevens in om te regristreren.</Text>
        <Form>
          <Item floatingLabel style={{ marginLeft: 10, marginRight: 10, }}>
            <Label>Inlog naam</Label>
            <Input onChangeText={this.handleInlognaam}/>
          </Item>
          <Item floatingLabel   style={{ marginLeft: 10, marginRight: 10, }}>
            <Label>Wachtwoord</Label>
            <Input secureTextEntry={true} onChangeText={this.handlePassword}/>
          </Item>
          <Item floatingLabel   style={{ marginLeft: 10, marginRight: 10, }}>
            <Label>Wachtwoord controlle</Label>
            <Input secureTextEntry={true} onChangeText={this.handleConfirmpassword}/>
          </Item>
          <Item floatingLabel style={{ marginLeft: 10, marginRight: 10, }}>
            <Label>Gebruikers naam</Label>
            <Input onChangeText={this.handleGebruikersnaam}/>
          </Item>
          <Item floatingLabel style={{ marginLeft: 10, marginRight: 10, }}>
            <Label>E-mail</Label>
            <Input email-address onChangeText={this.handleEmail}/>
          </Item>
          <Button onPress = {() => this.register(this.state.inlognaam, this.state.password, this.state.confirmpassword, this.state.gebruikersnaam, this.state.email)} primary rounded  block style={{ padding: 10, marginTop: 40, marginLeft: 10, marginRight: 10, }}><Text>Registreren</Text></Button>
        </Form>
      </Content>
    </Container>
    );
  }
}
