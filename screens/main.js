import React, { Component } from 'react';

import { ImageBackground, TouchableHighlight, Text, View, ScrollView, StyleSheet} from 'react-native';

import { Content, Grid, Col, Thumbnail} from 'native-base';
import { NavigationActions, StackActions } from 'react-navigation';

import PropTypes from 'prop-types';


export default class Main extends Component {

    constructor(props) {
        super(props)
      }
    render() {
        const profile_pic = require('../assets/profile.png');

        return (
            <Content contentContainerStyle={{ flex: 1 }}>
                <ScrollView style={{ flex: 1 }}>
                    <View style={{ flex: 1 }}>
                        <View style={{ flex: 1, alignItems: 'center', paddingTop: 50, paddingBottom: 50  }}>
                            <Text style={{ fontSize: 24, fontWeight: "bold" }}>Welkom terug</Text>
                            <Thumbnail style={{ alignSelf: "center", marginTop: 30, marginBottom: 30 }} large source={ profile_pic } />
                            <Text style={{ fontSize: 34, fontWeight: "bold" }}>Vincent Venhuizen!</Text>
                        </View>
                    </View>
                    <View style={{ flex: 1, backgroundColor: "#00ced1" }}>
                        <TouchableHighlight style={{ height: 200,}} onPress={() => this.props.navigation.navigate('DashboardScreen')}>        
                            <ImageBackground source={require('../assets/dashboardmdpi.png')} style={{width: '100%', flex: 1 }}>
                                <View style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center'}}>
                                    <Text style={{ fontSize: 40, fontWeight: "bold", color: "white" }}>Dashboard</Text>
                                </View>
                            </ImageBackground>
                        </TouchableHighlight>
                        
                        <TouchableHighlight style={{ height: 200, }} onPress={() => this.props.navigation.navigate('Oefeningen')}>        
                            <ImageBackground source={require('../assets/oefeningenmdpi.png')} style={{width: '100%', flex: 1 }}>
                                <View style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center'}}>
                                    <Text style={{ fontSize: 40, fontWeight: "bold", color: "white" }}>Oefeningen</Text>
                                </View>
                            </ImageBackground>
                        </TouchableHighlight>  

                        <TouchableHighlight style={{ height: 200,}} onPress={() => this.props.navigation.navigate('Sportprogramma')}>        
                            <ImageBackground source={require('../assets/sportprogrammamdpi.png')} style={{width: '100%', flex: 1 }}>
                                <View style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center'}}>
                                    <Text style={{ fontSize: 40, fontWeight: "bold", color: "white" }}>Sportprogramma's</Text>
                                </View>
                            </ImageBackground>
                        </TouchableHighlight>    
                    </View>
                </ScrollView>
            </Content>
        );
    }
}

module.exports = Main;