import React, { Component } from 'react';

import { StatusBar, AsyncStorage, Image } from 'react-native';

import { createDrawerNavigator, createAppContainer, createStackNavigator, NavigationActions, StackActions, Transitioner } from "react-navigation";
import { fromLeft } from 'react-navigation-transitions';

import { View } from 'native-base';

import { Entypo } from '@expo/vector-icons';

import { Font, AppLoading } from "expo";

import Sidebar from "./components/sidebar"
import Main from './screens/main';
import Login from './screens/login';
import Register from './screens/register';
import Oefeningen from './screens/oefeningen';
import SportProgramma from './screens/sportprogramma';
import nieuwSportprogramma from './screens/nieuwSportprogramma';
import startSportprogramma from './screens/programma';
import bekijkSportProgramma from './screens/bekijkSportprogramma';
import Settings from './screens/settings';
import Dashboard from './screens/dashboard';
import Oefening from './screens/oefening';

export function push (routeName, params) {
  return NavigationActions.navigate({
    routeName,
    params
  })
}


class App extends React.Component {
  constructor(props){
    super(props);
  }

  componentDidMount() {
    StatusBar.setHidden(true);
  }
  render() {
      return (
        <MyApp />
      );
    }
}

const MainStack = createStackNavigator({
    MainScreen: {
      screen: Main,
      navigationOptions: ({ navigation }) => ({
        headerTitle: <Image 
                        source={require('./assets/logo.png')}
                        resizeMode="cover"
                        style={{
                        width: 120,
                        height: 50,
                        resizeMode:'contain',}}
        />,
        headerTitleStyle:{
          marginTop: -5,
          textAlign:'center', alignSelf:'center',flex:1
        },
        headerLeft: (<Entypo size={32} onPress={() => navigation.openDrawer()} name='menu' style={{ paddingLeft: 20, color: "white" }} />),
        headerStyle:{
          marginTop: -20,
          backgroundColor: "#00ced1",
        },
        headerRight: (<View></View>)
      }),
    },
    DashboardScreen: {
        screen: Dashboard,
        navigationOptions: ({ navigation }) => ({
          title:'Dashboard',
          headerTitleStyle:{
            fontWeight:'500',
            fontSize:24,
            marginTop: -5,
            textAlign:'center',
            color: 'white',
          },
          headerLeft: (<Entypo size={32} onPress={() => navigation.goBack(null)} name='chevron-left' style={{ paddingLeft: 20, color: "white" }} />),
          headerStyle:{
            marginTop: -20,
            backgroundColor: "#00ced1",
          }
        }),
    },
})
const OefeningenStack = createStackNavigator({
  MainScreen: {
    screen: Oefeningen,
    navigationOptions: ({ navigation }) => ({
      title:'Oefeningen',
      headerTitleStyle:{
        fontWeight:'500',
        fontSize:24,
        marginTop: -5,
        textAlign:'center',
        color: 'white',
      },
      headerLeft: (<Entypo size={32} onPress={() => navigation.goBack(null)} name='chevron-left' style={{ paddingLeft: 20, color: "white" }} />),
      headerStyle:{
        marginTop: -20,
        backgroundColor: "#00ced1",
      }
    }),
  },
  SecondScreen:{
    screen: Oefening,
    navigationOptions: ({ navigation }) => ({
      
      headerTitleStyle:{
        fontWeight:'500',
        fontSize:24,
        marginTop: -5,
        textAlign:'center',
        color: 'white',
      },
      headerLeft: (<Entypo size={32} onPress={() => navigation.goBack(null)} name='chevron-left' style={{ paddingLeft: 20, color: "white" }} />),
      headerStyle:{
        marginTop: -20,
        backgroundColor: "#00ced1",
      }
    }),
  }
})
const SportProgrammaStack = createStackNavigator({
  MainScreen: {
    screen: SportProgramma,
    navigationOptions: ({ navigation }) => ({
      title:'Sportprogramma',
      headerTitleStyle:{
        fontWeight:'500',
        fontSize:24,
        marginTop: -5,
        textAlign:'center',
        color: 'white',
      },
      headerLeft: (<Entypo size={32} onPress={() => navigation.goBack(null)} name='chevron-left' style={{ paddingLeft: 20, color: "white" }} />),
      headerStyle:{
        marginTop: -20,
        backgroundColor: "#00ced1",
      }
    }),
  },

  nieuwProgramma:{
    screen: nieuwSportprogramma,
    navigationOptions: ({ navigation }) => ({
      title:'Nieuw sportprogramma',
      headerTitleStyle:{
        fontWeight:'500',
        fontSize:24,
        marginTop: -5,
        textAlign:'center',
        color: 'white',
      },
      headerLeft: (<Entypo size={32} onPress={() => navigation.goBack(null)} name='chevron-left' style={{ paddingLeft: 20, color: "white" }} />),
      headerStyle:{
        marginTop: -20,
        backgroundColor: "#00ced1",
      }
    }),
  },
  startProgramma:{
    screen: startSportprogramma,
    navigationOptions: ({ navigation }) => ({
      headerTitleStyle:{
        fontWeight:'500',
        fontSize:24,
        marginTop: -5,
        textAlign:'center',
        color: 'white',
      },
      headerLeft: (<Entypo size={32} onPress={() => navigation.goBack(null)} name='chevron-left' style={{ paddingLeft: 20, color: "white" }} />),
      headerStyle:{
        marginTop: -20,
        backgroundColor: "#00ced1",
      }
    }),
  },

  bekijkProgramma:{
    screen: bekijkSportProgramma,
    navigationOptions: ({ navigation }) => ({
      headerTitleStyle:{
        fontWeight:'500',
        fontSize:24,
        marginTop: -5,
        textAlign:'center',
        color: 'white',
      },
      headerLeft: (<Entypo size={32} onPress={() => navigation.goBack(null)} name='chevron-left' style={{ paddingLeft: 20, color: "white" }} />),
      headerStyle:{
        marginTop: -20,
        backgroundColor: "#00ced1",
      }
    }),
  }
})
const LoginStack = createStackNavigator({
  MainScreen: {
    screen: Login,
    navigationOptions: ({ navigation }) => ({
      title:'Login',
      headerTitleStyle:{
        fontWeight:'500',
        fontSize:24,
        marginTop: -5,
        textAlign:'center',
        color: 'white',
      },
      headerLeft: (<Entypo size={32} onPress={() => navigation.goBack(null)} name='chevron-left' style={{ paddingLeft: 20, color: "white" }} />),
      headerStyle:{
        marginTop: -20,
        backgroundColor: "#00ced1",
      }
    }),
  },
  RegisterScreen: {
    screen: Register,
    navigationOptions: ({ navigation }) => ({
      title:'Register',
      headerTitleStyle:{
        fontWeight:'500',
        fontSize:24,
        marginTop: -5,
        textAlign:'center',
        color: 'white',
      },
      headerLeft: (<Entypo size={32} onPress={() => navigation.goBack(null)} name='chevron-left' style={{ paddingLeft: 20, color: "white" }} />),
      headerStyle:{
        marginTop: -20,
        backgroundColor: "#00ced1",
      }
    }),
  }
})

const SettingStack = createStackNavigator({
  MainScreen: {
    screen: Settings,
    navigationOptions: ({ navigation }) => ({
      title:'Settings',
      headerTitleStyle:{
        fontWeight:'500',
        fontSize:24,
        marginTop: -5,
        textAlign:'center',
        color: 'white',
      },
      headerLeft: (<Entypo size={32} onPress={() => navigation.goBack(null)} name='chevron-left' style={{ paddingLeft: 20, color: "white" }} />),
      headerStyle:{
        marginTop: -20,
        backgroundColor: "#00ced1",
      }
    }),
  }
})





const MainDrawer = createDrawerNavigator({
    Home: {
      screen: MainStack, // you may want to use a stack here too
    },
    Oefeningen: {
      screen: OefeningenStack, // you may want to use a stack here too
    },
    Sportprogramma: {
      screen: SportProgrammaStack, // you may want to use a stack here too
    },
    Login: {
      screen: LoginStack, // you may want to use a stack here too
    },
    Settings: {
      screen: SettingStack,
    },
  },
  {
    contentComponent: Sidebar,
    portraitOnlyMode: true,
  }
)


const MyApp = createAppContainer(MainDrawer);

export default App
